/// A message constructer.
pub trait MessageBuilder {
    /// The message produced.
    type Message<'a>;
    /// The data used to construct the message.
    type Data<'a>;

    fn from<'a>(&self, data: Self::Data<'a>) -> Self::Message<'a>;
}

/// Convert a [`MessageBuilder`] into one that will build a request.
pub trait AsRequest: MessageBuilder {
    fn as_request(self) -> Self;
}

/// Request a message from a [`MessageBuilder`].
pub trait Request<D: MessageBuilder> {
    fn get<'a>(&'a self, desc: &D) -> D::Message<'a>;
}

/// Handle incomming messages.
pub trait MessageHandler<Message> {
    fn post(&mut self, message: Message);
}

/// Release data from an object.
pub trait Release<T> {
    fn release(self) -> T;
}

/// A message that delivers data associated to a field.
pub trait FieldParameter {}

pub trait Router<'a, M, D>
where
    M: MessageBuilder<Data<'a> = D>,
    Self: Request<M> + MessageHandler<M::Message<'a>>,
{
}

unsafe fn shorten_lifetime<'a, T>(this: &mut T) -> &'a mut T {
    let ptr: *mut T = this;
    &mut *ptr
}

/// Post data to a [`MessageHandler`].
pub fn post<'a, T, R>(this: &mut T, req: &R, data: R::Data<'a>) -> bool
where
    R: MessageBuilder,
    R::Message<'a>: PartialEq + FieldParameter,
    T: Request<R> + 'a,
    T: MessageHandler<R::Message<'a>>,
{
    let message = req.from(data);
    let cmp = unsafe { shorten_lifetime(this).get(req).eq(&message) };
    if !cmp {
        this.post(message);
    }
    !cmp
}
