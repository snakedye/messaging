use crate::api::*;

use std::marker::PhantomData;

#[derive(Clone, Copy)]
/// A simple message with the data from a [`MessageBuilder`].
pub struct Msg<'a, I>
where
    I: MessageBuilder,
{
    data: I::Data<'a>,
    _id: PhantomData<I>,
}

impl<'a, I: MessageBuilder> Msg<'a, I> {
    pub fn new(data: I::Data<'a>) -> Self {
        Self {
            data,
            _id: PhantomData,
        }
    }
    pub fn get(self) -> I::Data<'a> {
        self.data
    }
    pub fn map<F, U>(self, f: F) -> U
    where
        F: FnOnce(I::Data<'a>) -> U,
    {
        f(self.get())
    }
}

impl<'a, I: MessageBuilder> PartialEq for Msg<'a, I>
where
    I::Data<'a>: PartialEq,
{
    fn eq(&self, other: &Self) -> bool {
        self.data == other.data
    }
}

impl<'a, I: MessageBuilder> Release<I::Data<'a>> for Msg<'a, I> {
    fn release(self) -> I::Data<'a> {
        self.get()
    }
}

impl<'a, I: FieldParameter + MessageBuilder> FieldParameter for Msg<'a, I> {}

impl<'a, I: MessageBuilder, T> AsRef<T> for Msg<'a, I>
where
    T: ?Sized,
    I::Data<'a>: AsRef<T>,
{
    fn as_ref(&self) -> &T {
        self.data.as_ref()
    }
}

impl<'a, I: MessageBuilder, T> AsMut<T> for Msg<'a, I>
where
    T: ?Sized,
    I::Data<'a>: AsMut<T>,
{
    fn as_mut(&mut self) -> &mut T {
        self.data.as_mut()
    }
}

impl<'a, I: MessageBuilder> std::fmt::Debug for Msg<'a, I>
where
    I::Data<'a>: std::fmt::Debug,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let id = std::any::type_name::<I>();
        f.debug_struct("Msg")
            .field("id", &id)
            .field("data", &self.data)
            .finish()
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
/// A message with the [`MessageBuilder`] and its data.
pub struct RichMsg<'a, I>
where
    I: MessageBuilder,
{
    id: I,
    data: I::Data<'a>,
}

impl<'a, I: MessageBuilder> RichMsg<'a, I> {
    pub fn new(id: I, data: I::Data<'a>) -> Self {
        Self { data, id }
    }
    pub fn get(self) -> I::Data<'a> {
        self.data
    }
    pub fn id(&self) -> &I {
        &self.id
    }
    pub fn unwrap(self) -> (I, I::Data<'a>) {
        (self.id, self.data)
    }
    pub fn map<F, U>(self, f: F) -> U
    where
        F: FnOnce(I::Data<'a>) -> U,
    {
        f(self.get())
    }
}

impl<'a, I: MessageBuilder> Release<I::Data<'a>> for RichMsg<'a, I> {
    fn release(self) -> I::Data<'a> {
        self.get()
    }
}

impl<'a, I: FieldParameter + MessageBuilder> FieldParameter for RichMsg<'a, I> {}

impl<'a, I: MessageBuilder, T> AsRef<T> for RichMsg<'a, I>
where
    T: ?Sized,
    I::Data<'a>: AsRef<T>,
{
    fn as_ref(&self) -> &T {
        self.data.as_ref()
    }
}

impl<'a, I: MessageBuilder, T> AsMut<T> for RichMsg<'a, I>
where
    T: ?Sized,
    I::Data<'a>: AsMut<T>,
{
    fn as_mut(&mut self) -> &mut T {
        self.data.as_mut()
    }
}
