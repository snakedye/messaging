#[macro_export]
/// An ergonomic way to receive messages.
macro_rules! post {
    ($type:ty: $handler:expr, $msg:expr) => {
        <App as $crate::MessageHandler<message::Msg<'_, $type>>>::post(
            &mut $handler,
            $crate::message::Msg::new($msg),
        );
    };
}

#[macro_export]
/// Create a new message.
macro_rules! Message {
    (
        $(#[$meta:meta])*
    	$type:ident<$data:ty>
	) => {
        #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
        $(#[$meta])*
        pub struct $type;

        impl $crate::MessageBuilder for $type {
            type Message<'a> = $crate::message::Msg<'a, $type>;
            type Data<'a> = $data;

            fn from<'a>(&self, data: Self::Data<'a>) -> Self::Message<'a> {
                $crate::message::Msg::new(data)
            }
        }
    };
}

#[macro_export]
/// Create multiple messages.
macro_rules! Messages {
    ($($type:ident<$data:ty>),* $(,)?) => {
        $(
        	#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
            pub struct $type;

            impl $crate::MessageBuilder for $type {
                type Message<'a> = $crate::message::Msg<'a, $type>;
                type Data<'a> = $data;

                fn from<'a>(&self, data: Self::Data<'a>) -> Self::Message<'a> {
                    $crate::message::Msg::new(data)
                }
            }
        )*
    };
    ($type:ident<$data:ty> {$($choice:ident),* $(,)?}) => {
    	#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
        pub enum $type {
        	$($choice,)*
        }

        impl $crate::MessageBuilder for $type {
            type Message<'a> = $crate::message::RichMsg<'a, $type>;
            type Data<'a> = $data;

            fn from<'a>(&self, data: Self::Data<'a>) -> Self::Message<'a> {
                $crate::message::RichMsg::new(*self, data)
            }
        }
    };
}

#[macro_export]
/// Handle incomming messages.
macro_rules! MessageHandler {
    ($type:ty { $($pat:ty => ($this:tt, $msg:tt) $f:block)* }) => {
    	$(
            impl $crate::MessageHandler<$crate::message::Msg<'_, $pat>> for $type {
            	fn post(&mut self, $msg: $crate::message::Msg<'_, $pat>) {
            		let $this = self;
            		$f
            	}
            }
    	)*
    };
    ($type:ty { $($pat:ty => ($this:tt, $id:tt, $data:tt) $f:block)* }) => {
    	$(
            impl $crate::MessageHandler<$crate::message::RichMsg<'_, $pat>> for $type {
            	fn post(&mut self, msg: $crate::message::RichMsg<'_, $pat>) {
            		let $this = self;
            		let ($id, $data) = msg.unwrap();
            		$f
            	}
            }
    	)*
    };
    ($type:ty { $($pat:ty => ($this:tt, $msg:tt) $f:block)* }) => {
    	$(
            impl $crate::MessageHandler<$pat> for $type {
            	fn post(&mut self, $msg: $pat>) {
            		let $this = self;
            		$f
            	}
            }
    	)*
    };
}

#[macro_export]
/// Handle requests from [`MessageBuilder`].
macro_rules! RequestManager {
    ($type:ty { $($pat:ty => ($this:tt, $desc:tt) $f:block)* }) => {
        $(
        	impl $crate::Request<$pat> for $type {
                fn get<'a>(&'a self, $desc: &$pat) -> <$pat as $crate::MessageBuilder>::Message<'a> {
            		let $this = self;
            		$f
                }
        	}
        )*
    };
}
