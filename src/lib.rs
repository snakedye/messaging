pub(crate) mod api;
mod macros;
pub mod message;

pub use api::*;

#[cfg(test)]
mod tests {
    use super::*;

    pub struct App(String);

    Messages! {
        Color<u32> {
            Red,
            Blue,
        }
    }

    Messages! {
        Text<&'a str>,
        Scale<u32>
    }

    MessageHandler! {
        App {
            Text => (this, msg) {
                this.0.replace_range(0.., msg.get());
            }
            Scale => (_, msg) {
                println!("{:?}", msg.get());
            }
        }
    }

    RequestManager! {
        App {
            Text => (this, desc) {
                desc.from(&this.0)
            }
        }
    }

    impl FieldParameter for Text {}

    #[test]
    fn test_request() {
        let app = App(String::new());

        let message = app.get(&Text);

        assert_eq!(message.get(), "");
    }

    #[test]
    fn test_handler() {
        let mut app = App(String::new());

        let s = "hello world";

        post!(Text: app, s);

        assert_eq!(app.get(&Text).get(), s);
    }

    #[test]
    fn test_debug() {
        let mut app = App(String::new());

        post!(Scale: app, 10);
    }

    #[test]
    fn test_broker() {
        let mut app = App(String::new());

        assert_eq!(post(&mut app, &Text, "hello"), true);
        assert_eq!(post(&mut app, &Text, "hello"), false);
    }

    #[test]
    fn test_rich_msg() {
        let msg = Color::Blue.from(1);

        assert_eq!(*msg.id(), Color::Blue);
        assert_eq!(msg.get(), 1);

        let msg = Color::Red.from(2);
        assert_eq!(*msg.id(), Color::Red);
        assert_eq!(msg.get(), 2);
    }
}
